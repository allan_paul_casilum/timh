To Install / Setup

-1. clone the repo, "git clone https://allan_paul_casilum@bitbucket.org/allan_paul_casilum/timh.git"

-2. in your command line, run composer update

-3. in terminal type and run it (press enter): php artisan migrate

-4. in terminal type and run it (press enter) : php artisan db:seed

-5. update the .env for the database setup like name, username and password

-6. the mail is using mailtrap so update the mail config in the .env

-7. change the  app_url in the .env to your preferred or setup url (if env is not present, open the .env.example and save it as .env)

-8. go to your prefered or setup url; example: http://example.com/public

-9. username is admin@gmail.com password is secret