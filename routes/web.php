<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    //return view('welcome');
});*/



Auth::routes();
Route::get('logout',function(){
	Auth::logout();
	return redirect('/login');
});
Route::middleware(['auth'])->group(function () {
Route::get('/home', 'LicenseTrackerController@index')->name('home');
Route::get('/', 'LicenseTrackerController@index')->name('index');
Route::get('edit/{id}', 'LicenseTrackerController@edit')->name('edit_article');
Route::put('update', 'LicenseTrackerController@update')->name('update_article');
//Route::get('/sendreport','LicenseTrackerController@sendReport');
Route::post('/sendreport','LicenseTrackerController@sendReport');
Route::get('/test','LicenseTrackerController@test');
});