<?php

use Illuminate\Database\Seeder;

class LicenseTrackerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('license_tracker')->insert(
            array(
                array('us_states' => 'Alabama', 'us_states_code' => 'AL', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Alaska', 'us_states_code' => 'AK', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Arizona', 'us_states_code' => 'AZ', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Arkansas', 'us_states_code' => 'AR', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'California', 'us_states_code' => 'CA', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Colorado', 'us_states_code' => 'CO', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Connecticut', 'us_states_code' => 'CT', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Delaware', 'us_states_code' => 'DE', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'District of Columbia', 'us_states_code' => 'DC', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Florida', 'us_states_code' => 'FL', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Georgia', 'us_states_code' => 'GA', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Hawaii', 'us_states_code' => 'HI', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Idaho', 'us_states_code' => 'ID', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Illinois', 'us_states_code' => 'IL', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Indiana', 'us_states_code' => 'IN', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Iowa', 'us_states_code' => 'IA', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Kansas', 'us_states_code' => 'KS', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Kentucky', 'us_states_code' => 'KY', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Louisiana', 'us_states_code' => 'LA', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Maine', 'us_states_code' => 'ME', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Maryland', 'us_states_code' => 'MD', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Massachusetts', 'us_states_code' => 'MA', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Michigan', 'us_states_code' => 'MI', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Minnesota', 'us_states_code' => 'MN', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Mississippi', 'us_states_code' => 'MS', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Missouri', 'us_states_code' => 'MO', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Montana', 'us_states_code' => 'MT', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Nebraska', 'us_states_code' => 'NE', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Nevada', 'us_states_code' => 'NV', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'New Hampshire', 'us_states_code' => 'NH', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'New Jersey', 'us_states_code' => 'NJ', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'New Mexico', 'us_states_code' => 'NM', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'New York', 'us_states_code' => 'NY', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'North Carolina', 'us_states_code' => 'NC', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'North Dakota', 'us_states_code' => 'ND', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Ohio', 'us_states_code' => 'OH', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Oklahoma', 'us_states_code' => 'OK', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Oregon', 'us_states_code' => 'OR', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Pennsylvania', 'us_states_code' => 'PA', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Rhode Island', 'us_states_code' => 'RI', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'South Carolina', 'us_states_code' => 'SC', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'South Dakota', 'us_states_code' => 'SD', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Tennessee', 'us_states_code' => 'TN', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Texas', 'us_states_code' => 'TX', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Utah', 'us_states_code' => 'UT', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Vermont', 'us_states_code' => 'VT', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Virginia', 'us_states_code' => 'VA', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Washington', 'us_states_code' => 'WA', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'West Virginia', 'us_states_code' => 'WV', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Wisconsin', 'us_states_code' => 'WI', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 ),
                array('us_states' => 'Wyoming', 'us_states_code' => 'WY', 'license' => '', 'expiration_date' => \Carbon\Carbon::createFromDate(2000,01,01)->toDateTimeString(), 'valid' => 0 )
		));
    }
}
