@extends('layout')


@section('content')
    <h1>Expiring within 30 days</h1>
	<table class="table table-bordered">
        <tr>
            <th>State</th>
            <th>License Number</th>
            <th>Expiration Date</th>
            <th>Status</th>
        </tr>
    @foreach ($dbs as $db)
    <tr>
        <td>{{$db->us_states}}</td>
        <td>{{ $db->license}}</td>
        <td>{{ $db->expiration_date}}</td>
        <td>{{ ($db->license!='') ? 'License Found':'No License Found'}}</td>
    </tr>
    @endforeach
    </table>

@endsection