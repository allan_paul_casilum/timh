@extends('layout')


@section('content')
<h1></h1>
<p></p>
<p></p>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>License Tracker</h2>
            </div>
            <div class="pull-right">
               {!! Form::open(['url' => 'sendreport', 'class'=>'form-inline']) !!}
					<div class="form-group mx-sm-3">
						<label for="sendreport" class="sr-only">Enter Email Address</label>
						<input type="text" class="form-control" name="sendreport"id="sendreport" placeholder="Enter Email Address" value>
					  </div>
					  <button type="submit" class="btn btn-primary">Send Report</button>
				{!! Form::close() !!}
            </div>
        </div>
    </div>
	
	@if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>State</th>
            <th>License Number</th>
            <th>Expiration Date</th>
            <th>Status</th>
        </tr>
    @foreach ($dbs as $db)
    <tr>
        <td><a href="{{route('edit_article',['id'=>$db->id])}}">{{ $db->us_states}}</a></td>
        <td>{{ $db->license}}</td>
        <td>{{ $db->expiration_date}}</td>
        <td>{{ ($db->license!='') ? 'License Found':'No License Found'}}</td>
    </tr>
    @endforeach
    </table>

@endsection