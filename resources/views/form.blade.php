<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>State</strong>
            <h2>{{$db->us_states}}</h2>
        </div>
		<div class="form-group">
            <strong>License Number</strong>
			 {!! Form::text('license', null, array('placeholder' => 'License','class' => 'form-control')) !!}
        </div>
		<div class="form-group">
            <strong>License Number</strong>
			 {!! Form::date('expiration_date', \Carbon\Carbon::now(), array('placeholder' => 'Expiration Date','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>