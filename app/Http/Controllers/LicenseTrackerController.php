<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\LicenseTracker;
use App\Mail\NotifyReport;

class LicenseTrackerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$dbs = LicenseTracker::all();
		return view('index',compact('dbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $db = LicenseTracker::find($id);
        return view('edit',compact('db'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		
		$id = $request->input('id');
        request()->validate([
            'license' => 'required',
            'expiration_date' => 'required',
        ]);
		LicenseTracker::find($id)->update($request->all());
		return redirect()->route('index')
                        ->with('success','Article updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function test(){
		$db = LicenseTracker::where('expiration_date', '<=', \Carbon\Carbon::now()->subMonth())->get();
		foreach($db as $_db){
			echo $_db->us_states.'<br>';
		}
	}
	
	public function sendReport(Request $request){
		$email_to = $request->input('sendreport');
		request()->validate([
            'sendreport' => 'required',
        ]);
		$notify = LicenseTracker::where('expiration_date', '<=', \Carbon\Carbon::now()->subMonth())->get();
		Mail::to($email_to)->send(new NotifyReport($notify));
		return redirect()->route('index')
                        ->with('success','Report Sent');
	}
}
