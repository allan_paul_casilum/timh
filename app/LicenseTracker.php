<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LicenseTracker extends Model
{
    protected $table = 'license_tracker';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'us_states', 'us_states_code', 'license', 'expiration_date', 'valid'
    ];

}
